import cocos
from enum import Enum
from cocos.layer import *
from cocos.sprite import *
from cocos.director import director
from cocos.actions import *
from cocos.cocosnode import *
from cocos.text  import *
from pyglet import font
from pyglet.window import key as keys
# 1 - стена, 0 - шар, 2 - таблетка, 3 - чернота
l = '''111111111111111111111
100000000010000000001
101110111010111011101
121310131010131013121
101110111010111011101
100000000000000000001
101110101111101011101
101110101111101011101
100000100010001000001
111110111313111011111
333310133333331013333
333310131111131013333
111110131333131011111
333330331333133033333
111110131111131011111
333310133333331013333
333310131111131013333
111110131111131011111
100000000010000000001
101110111010111011101
120010000030000010021
111010101111101010111
111010101111101010111
100000100010001000001
101111111010111111101
100000000000000000001
111111111111111111111
'''
Game_status=True
big_list=l.split('\n')

class Direction(Enum):
    RIGHT=(1,0)
    LEFT=(-1,0)
    UP=(0,1)
    DOWN=(0,-1)
    NONE=(0,0)
    
class HUD(Layer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.score = 0
        self.label2 = Label(str('your score:'), position=(10,905), font_name="Times New Roman", font_size=32, color=(152,152,152,255))
        self.add(self.label2)
        self.label = Label(str(self.score), position=(210,905), font_name="Times New Roman", font_size=32, color=(152,152,152,255))
        self.add(self.label)

class PlayerInput(Layer):
    is_event_handler = True
    def __init__(self, k):
        super().__init__()
        self.keyable = True
    def allow_movement(self):
        self.keyable = True
    def on_key_press(self, key, modifiers):
        if self.keyable:
            if key==keys.W:
                self.parent.possible_direction=Direction.UP.value
                if ((self.parent.pacman.x, self.parent.pacman.y+32) not in self.parent.wall_set):
                    self.parent.pacman.direction=Direction.UP.value                
            if key==keys.A:
                self.parent.possible_direction=Direction.LEFT.value
                if ((self.parent.pacman.x-32, self.parent.pacman.y) not in self.parent.wall_set):
                    self.parent.pacman.direction=Direction.LEFT.value
            if key==keys.D:
                self.parent.possible_direction=Direction.RIGHT.value
                if ((self.parent.pacman.x+32, self.parent.pacman.y) not in self.parent.wall_set):
                    self.parent.pacman.direction=Direction.RIGHT.value
            if key==keys.S:
                self.parent.possible_direction=Direction.DOWN.value
                if ((self.parent.pacman.x, self.parent.pacman.y-32) not in self.parent.wall_set):
                    self.parent.pacman.direction=Direction.DOWN.value
                
class Level1(cocos.scene.Scene):
    def __init__(self):
        super().__init__()
        count_y=27
        k=0
        self.possible_direction = Direction.NONE.value
        self.wall_set=set()
        self.dot_set=set()
        self.Background = cocos.layer.util_layers.ColorLayer(0, 0, 0, 255, width=700, height=890)
        self.add(self.Background)
        self.Pacman_layer = cocos.layer.util_layers.ColorLayer(0, 0, 0, 0, width=700, height=890)
        self.add(self.Pacman_layer)
        self.h=HUD()
        self.add(self.h)
        for short_list in big_list:
            part=list(short_list)
            count_y-=1
            count_x=0
            for element in part:
                count_x+=1
                if element=='1':
                    self.cell = cocos.sprite.Sprite('pics/wall.png', position=(32*count_x,32+32*count_y), opacity=255)
                    self.Background.add(self.cell)
                    self.wall_set.add((32*count_x,32+32*count_y))
                elif element=='0':
                    self.cell = cocos.sprite.Sprite('pics/dot.png', position=(32*count_x,32+32*count_y), opacity=255)
                    self.Background.add(self.cell)
                    self.dot_set.add((32*count_x,32+32*count_y))
                elif element=='2':
                    self.cell = cocos.sprite.Sprite('pics/tablet.png', position=(32*count_x,32+32*count_y), opacity=255)
                    self.Background.add(self.cell)
                elif element=='3':
                    self.cell = cocos.sprite.Sprite('pics/black.png', position=(32*count_x,32+32*count_y), opacity=255)
                    self.Background.add(self.cell)
        self.pacman = cocos.sprite.Sprite('pics/pacman.png', position=(352,32+192), opacity=255)
        self.Pacman_layer.add(self.pacman)
        self.pacman.direction=Direction.RIGHT.value
        x, y=self.pacman.direction
        self.schedule_interval(self.update, 0.25)
        self.a = PlayerInput(k)
        self.add(self.a)
    def update(self, dt):
        x, y=self.pacman.direction
        if self.possible_direction == Direction.NONE.value:
            x, y=self.pacman.direction
            if (self.pacman.x+32*x, self.pacman.y+32*y) not in self.wall_set:
                self.pacman.x+=32*x
                self.pacman.y+=32*y
        else:
            x, y = self.possible_direction
            if (self.pacman.x+32*x, self.pacman.y+32*y) not in self.wall_set:
                self.pacman.x+=32*x
                self.pacman.y+=32*y
                self.pacman.direction = self.possible_direction
            else:
                x, y=self.pacman.direction
                if (self.pacman.x+32*x, self.pacman.y+32*y) not in self.wall_set:
                    self.pacman.x+=32*x
                    self.pacman.y+=32*y         
        if self.pacman.y==448:
            if self.pacman.x==704:
                self.pacman.x=32
            if self.pacman.x==0:
                self.pacman.x=672
        if (self.pacman.x,self.pacman.y) in self.dot_set:
            self.dot_set.remove((self.pacman.x,self.pacman.y))
            self.dead_cell = cocos.sprite.Sprite('pics/black.png', position=(self.pacman.x,self.pacman.y), opacity=255)
            self.Background.add(self.dead_cell)
            self.h.score+=10
            self.h.label.element.text = str(self.h.score)
cocos.director.director.init(caption='PACMAN', width=700, height=950)
director.run(Level1())
